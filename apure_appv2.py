import sys
import re
import pandas as pd
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class App(QWidget):

    def __init__(self):
        super(App, self).__init__()
        self.leftlist = QListWidget ()
        self.leftlist.insertItem (0, 'Offline Events' )
        self.leftlist.insertItem (1, 'Purchase Type Products' )
        self.leftlist.insertItem (2, 'Members and App - Counting and Expenses' )
        self.leftlist.insertItem (3, '91App to 宅配' )

        self.stack1 = QWidget()
        self.stack2 = QWidget()
        self.stack3 = QWidget()
        self.stack4 = QWidget()

        self.stack1UI()
        self.stack2UI()
        self.stack3UI()
        self.stack4UI()

        self.Stack = QStackedWidget (self)
        self.Stack.addWidget (self.stack1)
        self.Stack.addWidget (self.stack2)
        self.Stack.addWidget (self.stack3)
        self.Stack.addWidget (self.stack4)

        hbox = QHBoxLayout(self)
        hbox.addWidget(self.leftlist)
        hbox.addWidget(self.Stack)

        self.setLayout(hbox)
        self.leftlist.currentRowChanged.connect(self.display)
        self.setGeometry(700, 300, 400,10)
        self.setWindowTitle('aPure Data Processing')
        self.show()
		
    def stack1UI(self):
        layout = QFormLayout()
        button = QPushButton('To Offline Event', self)
        button.move(150,50)
        button.clicked.connect(self.on_click_offline)
        layout.addWidget(button)
        self.stack1.setLayout(layout)
        
    def stack2UI(self):
        layout = QFormLayout()
        button = QPushButton('Upload 91 Data', self)
        button.move(150,50)
        button.clicked.connect(self.on_click_purchase)
        layout.addWidget(button)
        self.stack2.setLayout(layout)
        
    def stack3UI(self):
        layout = QFormLayout()
        button1 = QPushButton('With Members', self)
        button2 = QPushButton('With Non Members', self)
        button3 = QPushButton('Members Using App', self)
        button1.move(150,10)
        button2.move(150,40)
        button3.move(150,70)
        button1.clicked.connect(self.on_click_with_members)
        button2.clicked.connect(self.on_click_with_nonmembers)
        button3.clicked.connect(self.on_click_app_members)
        layout.addWidget(button1)
        layout.addWidget(button2)
        layout.addRow(button3)
        self.stack3.setLayout(layout)

    def stack4UI(self):
        layout = QFormLayout()
        button = QPushButton('Upload 91 Data', self)
        button.move(150,50)
        button.clicked.connect(self.on_click_to_zaipei)
        layout.addWidget(button)
        self.stack4.setLayout(layout)

    def display(self,i):
        self.Stack.setCurrentIndex(i)

    def getFile(self):
        options = QFileDialog.Options()
        fname = QFileDialog.getOpenFileName(self, 'Choose file', options=options)
        if fname:
            return fname

    @pyqtSlot()
    def on_click_offline(self):
        filename = self.getFile()
        if filename[0] != '':
            formated_file = format(filename[0])
            new_filename = filename[0].split('.')[0]+'.csv'
            formated_file.to_csv(new_filename, sep=',', encoding='utf-8-sig', index=False)

    @pyqtSlot()
    def on_click_purchase(self):
        filename = self.getFile()
        if filename[0] != '':
            formated_file = ptp(filename[0])
            new_filename = filename[0].split('.')[0]+'.csv'
            formated_file.to_csv(new_filename, sep=',', encoding='utf-8-sig')
    @pyqtSlot()
    def on_click_with_members(self):
        self.filename1 = self.getFile()
            
    @pyqtSlot()
    def on_click_with_nonmembers(self):
        self.filename2 = self.getFile()

    @pyqtSlot()
    def on_click_app_members(self):
        self.filename3 = self.getFile()
        if self.filename3[0] != '' and self.filename1[0] != '' and self.filename2[0] != '':
            self.myThread = CustomerTrackingThread(self.filename1, self.filename2, self.filename3)
            formated_file = self.myThread.start()
            #formated_file = customer_tracking(self.filename1[0], self.filename2[0], filename[0])
            
    @pyqtSlot()
    def on_click_to_zaipei(self):
        filename = self.getFile()
        if filename[0] != '':
            formated_file = _91_to_zaipei(filename[0])
            new_filename = filename[0].split('.')[0]+'.csv'
            formated_file.to_csv(new_filename, sep=',', encoding='utf-8-sig', index=False)
            
            formated_file['address'] = formated_file['district']+formated_file['address']
            del formated_file['district']
            new_filename = filename[0].split('.')[0]+'_second.csv'
            formated_file.to_csv(new_filename, sep=',', encoding='utf-8-sig', index=False)

class CustomerTrackingThread(QThread):

    def __init__(self, filename1, filename2, filename3):
        QThread.__init__(self)
        self.filename1 = filename1
        self.filename2 = filename2
        self.filename3 = filename3

    def __del__(self):
        self.wait()

    def run(self):
        withMembers = pd.read_excel(self.filename1[0])
        withMembers_order_id = withMembers[u'會員日期商品消費明細表.5'] + '-' + withMembers[u'會員日期商品消費明細表.3']
        withMembers_order_id = [x.replace(' ', '') for x in withMembers_order_id]

        withMembers_dataset = pd.DataFrame({'date': withMembers[u'會員日期商品消費明細表'], 
                                    'name': withMembers[u'會員日期商品消費明細表.2'],
                                            'payment_origin': withMembers[u'會員日期商品消費明細表.3'],
                                            'expenses': withMembers[u'會員日期商品消費明細表.12'],
                                'phone_or_id_customers': withMembers[u'會員日期商品消費明細表.1'],
                            'order_id': withMembers_order_id,
                                #'id': withMembers[u'會員日期商品消費明細表.5']
                            })

        withMembers_dataset = withMembers_dataset.loc[3:]
        withMembers_dataset = withMembers_dataset.loc[withMembers_dataset['date'] != '日期']
        withMembers_dataset = withMembers_dataset.loc[withMembers_dataset['date'] != '合計 :']
        withMembers_dataset = withMembers_dataset.loc[withMembers_dataset['payment_origin'] != '合計:']


        del withMembers_dataset['date']

        ## With Non Members
        withNonMembers = pd.read_excel(self.filename2[0])

        withNonMembers_order_id = withNonMembers[u'店櫃商品銷貨明細表.4'] + '-' + withNonMembers[u'店櫃商品銷貨明細表.5']
        withNonMembers_order_id = [x.replace(' ', '') for x in withNonMembers_order_id]
        empty_phone_or_id = []
        for x in withNonMembers_order_id:
            empty_phone_or_id.append(' ')
            
        withNonMembers_dataset = pd.DataFrame({'date': withNonMembers[u'店櫃商品銷貨明細表.3'], 
                                                'name': withNonMembers[u'店櫃商品銷貨明細表.6'],
                                'payment_origin': withNonMembers[u'店櫃商品銷貨明細表.5'],
                            'expenses': withNonMembers[u'店櫃商品銷貨明細表.12'],
                                'phone_or_id_customers': empty_phone_or_id,
                            'order_id': withNonMembers_order_id,
                                #'id': withNonMembers[u'店櫃商品銷貨明細表.4']
                            })

        withNonMembers_dataset = withNonMembers_dataset.loc[3:]
        withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['payment_origin'] != '小計:']
        withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['payment_origin'] != '合計:']
        withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['date'] != '小計 :']
        withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['date'] != '合計 :']

        del withNonMembers_dataset['date']



        ## Append members in non members
        pos_dataset = withNonMembers_dataset.append(withMembers_dataset)

        ## Divide data in 2
        phone_or_id_customers_order_id = pd.DataFrame({
            'phone_or_id_customers': pos_dataset['phone_or_id_customers'],
            'order_id': pos_dataset['order_id'],
            'payment_origin': pos_dataset['payment_origin'],
            'name': pos_dataset['name']
        })
        del pos_dataset['phone_or_id_customers']
        del pos_dataset['payment_origin']
        del pos_dataset['name']

        ## Aggregate to have sum for each order 
        pos_dataset = pos_dataset.groupby('order_id').agg('sum')

        pos_dataset = pos_dataset.reset_index()

        ## Classify members and non members
        membership = []
        members = withMembers_dataset['order_id'].tolist()
        phone_or_id_customers = []
        payment_origin = []
        name = []
        for key in pos_dataset['order_id']:
            other_info = phone_or_id_customers_order_id.loc[phone_or_id_customers_order_id['order_id'] == key]
            other_info_phone = other_info.loc[other_info['phone_or_id_customers'] !=' ']
            if not other_info_phone.empty:
                phone_or_id_customers.append(other_info_phone['phone_or_id_customers'].iloc[0])     
            else:
                phone_or_id_customers.append(' ')
            payment_origin.append(other_info['payment_origin'].iloc[0])
            name.append(other_info['name'].iloc[0])
            
            if key in members:
                membership.append("member")
            else:
                membership.append("non member")

        pos_dataset['membership'] = membership 
        pos_dataset['payment_origin'] = payment_origin 
        pos_dataset['phone_or_id_customers'] = phone_or_id_customers 
        pos_dataset['name'] = name 

        del pos_dataset['order_id']
        ## Get customers who are registered to 91 app
        _91_customers = pd.read_excel(self.filename3[0])
        customers_phone = [x[1:] for x in _91_customers['手機號碼']]
        app_not_app = []

        ## Detect those this day who are registered 91 or not
        app_not_app = []
        for c in pos_dataset['phone_or_id_customers']:
            if c in customers_phone:
                app_not_app.append('APP')
            else:
                app_not_app.append('NOAPP')

        pos_dataset['91APP'] = app_not_app

        new_key = pos_dataset['membership'] +'-'+pos_dataset['91APP']

        pos_dataset['key'] = new_key
        count = [1 for x in range(len(pos_dataset))]

        pos_dataset['count'] = count

        pivot_table_count = pos_dataset.pivot_table(index=["payment_origin", "name"], columns="key", aggfunc="count", values="count")
        pivot_table_count = pivot_table_count.fillna(' ')
        pivot_table_count = pivot_table_count.reset_index()
        pivot_table_count['new_payment_origin'] =  [ x+'-'+'Count_members_app' for x in pivot_table_count['payment_origin']]

        pivot_table_sum = pos_dataset.pivot_table(index=["payment_origin", "name"], columns="key", aggfunc="sum", values="expenses")
        pivot_table_sum = pivot_table_sum.fillna(' ')
        pivot_table_sum = pivot_table_sum.reset_index()
        pivot_table_sum['new_payment_origin'] =  [ x+'-'+'Sum_expenses' for x in pivot_table_sum['payment_origin']]

        #pivot_table.to_csv('pivot.csv', sep=',')


        data_concat = pd.concat([pivot_table_count, pivot_table_sum])
        data_concat = data_concat.reset_index()
        data_concat = data_concat.sort_values('payment_origin')
        del data_concat['index']
        data_concat = data_concat.reset_index()
        del data_concat['index']
        data_concat = data_concat.set_index(['payment_origin', 'new_payment_origin'])
        
        new_filename1 = self.filename1[0].split('.')[0]
        new_filename2 = self.filename2[0].split('.')[0].split('/')[-1]
        new_filename = new_filename1 +'_'+ new_filename2 +'.csv'
        data_concat.to_csv(new_filename, sep=',', encoding='utf-8-sig')
        # return data_concat

def _91_to_zaipei(filename):
    _91data = pd.read_excel(filename)

    useful_dataset = pd.DataFrame({'主單編號': _91data['主單編號'], 
                                '收件人': _91data['收件人'],
                                    '收件人電話': _91data['收件人電話'],
                                        '地址': _91data['地址'],
                            '商品名稱': _91data['商品名稱'],
                                '商品選項': _91data['商品選項'],
                        '商品料號': _91data['商品料號'],
                            '數量': _91data['數量'],
                                '銷售金額(折扣後)': _91data['銷售金額(折扣後)'],
                                '訂購備註': _91data['訂購備註'],
                                '購物車編號': _91data['購物車編號'],
                                '交期': _91data['交期'],
                                '配送方式': _91data['配送方式']
                                
                        })
    useful_dataset['收件人電話'] = [x[1:] for x in useful_dataset['收件人電話']]
    useful_dataset['商品料號'] = [x[1:] for x in useful_dataset['商品料號']]
    useful_dataset['province'] = [x[3:6] for x in useful_dataset['地址']]
    useful_dataset['district'] = [x[6:9] for x in useful_dataset['地址']]
    useful_dataset['address'] = [x[9:] for x in useful_dataset['地址']]
    del useful_dataset['地址']

    dataset_to_readjust = useful_dataset.loc[useful_dataset['銷售金額(折扣後)']%useful_dataset['數量'] != 0]
    useful_dataset = useful_dataset.loc[useful_dataset['銷售金額(折扣後)']%useful_dataset['數量'] == 0]

    dataset_to_readjust = dataset_to_readjust.reset_index()

    del dataset_to_readjust['index']

    dataset_to_readjust_copy = pd.DataFrame({'主單編號': [], 
                                '收件人': [],
                                    '收件人電話': [],
                                        'province':[],
                                'district': [],
                                'address': [],
                            '商品名稱': [],
                                '商品選項': [],
                        '商品料號': [],
                            '數量': [],
                                '銷售金額(折扣後)':[],
                                '訂購備註': [],
                                '購物車編號': [],
                                '交期': [],
                                '配送方式': []
                        })

    for i in range(len(dataset_to_readjust)):
        new_row = pd.DataFrame({'主單編號': [dataset_to_readjust.iloc[i]['主單編號']], 
                                '收件人': [dataset_to_readjust.iloc[i]['收件人']],
                                    '收件人電話': [dataset_to_readjust.iloc[i]['收件人電話']],
                                        'province': [dataset_to_readjust.iloc[i]['province']],
                                'district': [dataset_to_readjust.iloc[i]['district']],
                                'address': [dataset_to_readjust.iloc[i]['address']],
                            '商品名稱': [dataset_to_readjust.iloc[i]['商品名稱']],
                                '商品選項': [dataset_to_readjust.iloc[i]['商品選項']],
                        '商品料號': [dataset_to_readjust.iloc[i]['商品料號']],
                            '數量': ['1'],
                                '銷售金額(折扣後)':[dataset_to_readjust.iloc[i]['銷售金額(折扣後)']//dataset_to_readjust.iloc[i]['數量']],
                                '訂購備註': [dataset_to_readjust.iloc[i]['訂購備註']],
                                '購物車編號': [dataset_to_readjust.iloc[i]['購物車編號']],
                                '交期': [dataset_to_readjust.iloc[i]['交期']],
                                '配送方式': [dataset_to_readjust.iloc[i]['配送方式']]
                        })
        
        new_row = pd.concat([new_row]*int(dataset_to_readjust.iloc[i]['數量']))
        new_row = new_row.reset_index()
        del new_row['index']
        new_row.loc[new_row.index == 0, '銷售金額(折扣後)'] = new_row.iloc[0]['銷售金額(折扣後)'] + int(dataset_to_readjust.iloc[i]['銷售金額(折扣後)']%dataset_to_readjust.iloc[i]['數量'])
        dataset_to_readjust_copy = dataset_to_readjust_copy.append(new_row)

    useful_dataset_readjusted = pd.concat([dataset_to_readjust_copy, useful_dataset], sort=True)

    useful_dataset_readjusted = useful_dataset_readjusted.reset_index()

    del useful_dataset_readjusted['index']

    useful_dataset_readjusted = useful_dataset_readjusted[['主單編號', '收件人', '收件人電話', 'province', 'district','address','商品名稱','商品料號', '商品選項','數量', '銷售金額(折扣後)', '訂購備註']]

    useful_dataset_readjusted = useful_dataset_readjusted.sort_values('主單編號').reset_index()

    del useful_dataset_readjusted['index']
    useful_dataset_readjusted['收件人電話'] = ['\''+x for x in useful_dataset_readjusted['收件人電話']]
    
    return useful_dataset_readjusted

def format(filename):
    purchases = pd.read_excel(filename)

    date = purchases[u'會員日期商品消費明細表']
    date = [d.replace('/', '-')+'T22:00:00Z' for d in date]
    new_date = []
    for d in date:
        if d.split('-')[0].isnumeric():
            new_date.append(d.replace(d.split('-')[0], str(int(d.split('-')[0]) + 1911)))
            #date_split.append(str(int(d.split('-')[0]) + 1911))
        else:
            new_date.append(d)
    currency = ['TWD' for i in date]
    country = ['TW' for i in date]
    event_name = ['Purchase' for i in date]
    phone = purchases[u'會員日期商品消費明細表.1']
    phone = [re.sub(r'^09', '+8869', p) for p in phone]
    name = purchases[u'會員日期商品消費明細表.2']
    ln = [n for n in name]
    payment_origin = purchases[u'會員日期商品消費明細表.3']
    payment_number = purchases[u'會員日期商品消費明細表.5']
    #product = purchases[u'會員日期商品消費明細表.6']
    expense = purchases[u'會員日期商品消費明細表.12']
    order_id = purchases[u'會員日期商品消費明細表.5'] + '-' + purchases[u'會員日期商品消費明細表.3']
    dataset = pd.DataFrame({'event_time': new_date, 
                            'phone': phone, 
                        #'name': name, 
                            'payment_origin': payment_origin,
                            'ln': ln,
                        #'product': product,
                        'value': expense,
                        'order_id': order_id,
                        'country': country,
                        'currency': currency,
                        'event_name': event_name})

    dataset = dataset.loc[3:]
    dataset = dataset.loc[dataset['event_time'] != u'日期T22:00:00Z']
    dataset = dataset.loc[dataset['event_time'] != u'合計 :T22:00:00Z']
    dataset = dataset.loc[dataset['payment_origin'] != u'官網']
    dataset = dataset.loc[dataset['payment_origin'] != u'91APP移動官網']
    #dataset = dataset.loc[dataset['value'] > 0]
    del dataset['payment_origin']
    unique_phone = list(set(dataset['phone']))
    unique_dataset = pd.DataFrame()
    for p in unique_phone:
        subdat = dataset.loc[dataset['phone'] == p]
        values = subdat['value']  
        subdat.iloc[0]['value'] = sum(subdat['value'])
        unique_dataset = unique_dataset.append(subdat.iloc[0])
        #print(subdat.iloc[0]['value'])
    unique_dataset = unique_dataset.loc[unique_dataset['value'] > 0]
    return unique_dataset

def ptp(filename):
    purchases = pd.read_excel(filename)

    purchase_id = purchases[u'購物車編號']
    product_sku = purchases[u'商品料號']
    number_items = purchases[u'數量']

    # clean content
    cleaned_product_sku = []
    classified = []
    s10 = []
    v05 = []
    v08_09 = []
    for s in product_sku:
        cleaned_product_sku.append(s[1:])
        if s[1:].startswith('P') or s[1:].startswith('U'):
            classified.append('發熱衣') 
        elif s[1:].startswith('S'):
            classified.append('襪子') 
        elif s[1:].startswith('V01') or s[1:].startswith('V05'):
            classified.append('男內褲') 
        elif s[1:].startswith('V07') or s[1:].startswith('V08') or s[1:].startswith('V09'):
            classified.append('女內褲') 
        elif s[1:].startswith('T09'):
            classified.append('feGood') 
        else:
            classified.append('酸鹼平衡衣') 

    for s in product_sku:
        if s[1:].startswith('S10'):
            s10.append(s[1:])
        elif s[1:].startswith('V05'):
            v05.append(s[1:])
        elif s[1:].startswith('V08') or s[1:].startswith('V09'):
            v08_09.append(s[1:])
        else:
            pass
                
    dataset = pd.DataFrame({'id': purchase_id, 
                            'sku': cleaned_product_sku, 
                            'classified': classified, 
                            'number': number_items})

    dataset_s10 = dataset.loc[dataset['sku'].isin(s10)].copy()
    dataset_v05 = dataset.loc[dataset['sku'].isin(v05)].copy()
    dataset_v08_09 = dataset.loc[dataset['sku'].isin(v08_09)].copy()

    dataset_s10.loc[:,'classified'] = '蓄熱襪'
    dataset_v05.loc[:,'classified'] = '男三角褲'
    dataset_v08_09.loc[:,'classified'] = '女無痕褲'

    dataset = dataset.append(dataset_s10, ignore_index=True)
    dataset = dataset.append(dataset_v05, ignore_index=True)
    dataset = dataset.append(dataset_v08_09, ignore_index=True)

    #dataset.pivot(index="key", columns="classified")
    dataset = dataset.pivot_table(index="id", columns="classified", aggfunc="sum")
    dataset.fillna(' ')
    return dataset

def customer_tracking(filename1, filename2, filename3):
    withMembers = pd.read_excel(filename1)
    withMembers_order_id = withMembers[u'會員日期商品消費明細表.5'] + '-' + withMembers[u'會員日期商品消費明細表.3']
    withMembers_order_id = [x.replace(' ', '') for x in withMembers_order_id]

    withMembers_dataset = pd.DataFrame({'date': withMembers[u'會員日期商品消費明細表'], 
                                'name': withMembers[u'會員日期商品消費明細表.2'],
                                        'payment_origin': withMembers[u'會員日期商品消費明細表.3'],
                                        'expenses': withMembers[u'會員日期商品消費明細表.12'],
                            'phone_or_id_customers': withMembers[u'會員日期商品消費明細表.1'],
                        'order_id': withMembers_order_id,
                            #'id': withMembers[u'會員日期商品消費明細表.5']
                        })

    withMembers_dataset = withMembers_dataset.loc[3:]
    withMembers_dataset = withMembers_dataset.loc[withMembers_dataset['date'] != '日期']
    withMembers_dataset = withMembers_dataset.loc[withMembers_dataset['date'] != '合計 :']
    withMembers_dataset = withMembers_dataset.loc[withMembers_dataset['payment_origin'] != '合計:']


    del withMembers_dataset['date']

    ## With Non Members
    withNonMembers = pd.read_excel(filename2)

    withNonMembers_order_id = withNonMembers[u'店櫃商品銷貨明細表.4'] + '-' + withNonMembers[u'店櫃商品銷貨明細表.5']
    withNonMembers_order_id = [x.replace(' ', '') for x in withNonMembers_order_id]
    empty_phone_or_id = []
    for x in withNonMembers_order_id:
        empty_phone_or_id.append(' ')
        
    withNonMembers_dataset = pd.DataFrame({'date': withNonMembers[u'店櫃商品銷貨明細表.3'], 
                                            'name': withNonMembers[u'店櫃商品銷貨明細表.6'],
                            'payment_origin': withNonMembers[u'店櫃商品銷貨明細表.5'],
                        'expenses': withNonMembers[u'店櫃商品銷貨明細表.12'],
                            'phone_or_id_customers': empty_phone_or_id,
                        'order_id': withNonMembers_order_id,
                            #'id': withNonMembers[u'店櫃商品銷貨明細表.4']
                        })

    withNonMembers_dataset = withNonMembers_dataset.loc[3:]
    withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['payment_origin'] != '小計:']
    withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['payment_origin'] != '合計:']
    withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['date'] != '小計 :']
    withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['date'] != '合計 :']

    del withNonMembers_dataset['date']



    ## Append members in non members
    pos_dataset = withNonMembers_dataset.append(withMembers_dataset)

    ## Divide data in 2
    phone_or_id_customers_order_id = pd.DataFrame({
        'phone_or_id_customers': pos_dataset['phone_or_id_customers'],
        'order_id': pos_dataset['order_id'],
        'payment_origin': pos_dataset['payment_origin'],
        'name': pos_dataset['name']
    })
    del pos_dataset['phone_or_id_customers']
    del pos_dataset['payment_origin']
    del pos_dataset['name']

    ## Aggregate to have sum for each order 
    pos_dataset = pos_dataset.groupby('order_id').agg('sum')

    pos_dataset = pos_dataset.reset_index()

    ## Classify members and non members
    membership = []
    members = withMembers_dataset['order_id'].tolist()
    phone_or_id_customers = []
    payment_origin = []
    name = []
    for key in pos_dataset['order_id']:
        other_info = phone_or_id_customers_order_id.loc[phone_or_id_customers_order_id['order_id'] == key]
        other_info_phone = other_info.loc[other_info['phone_or_id_customers'] !=' ']
        if not other_info_phone.empty:
            phone_or_id_customers.append(other_info_phone['phone_or_id_customers'].iloc[0])     
        else:
            phone_or_id_customers.append(' ')
        payment_origin.append(other_info['payment_origin'].iloc[0])
        name.append(other_info['name'].iloc[0])
        
        if key in members:
            membership.append("member")
        else:
            membership.append("non member")

    pos_dataset['membership'] = membership 
    pos_dataset['payment_origin'] = payment_origin 
    pos_dataset['phone_or_id_customers'] = phone_or_id_customers 
    pos_dataset['name'] = name 

    del pos_dataset['order_id']
    ## Get customers who are registered to 91 app
    _91_customers = pd.read_excel(filename3)
    customers_phone = [x[1:] for x in _91_customers['手機號碼']]
    app_not_app = []

    ## Detect those this day who are registered 91 or not
    app_not_app = []
    for c in pos_dataset['phone_or_id_customers']:
        if c in customers_phone:
            app_not_app.append('APP')
        else:
            app_not_app.append('NOAPP')

    pos_dataset['91APP'] = app_not_app

    new_key = pos_dataset['membership'] +'-'+pos_dataset['91APP']

    pos_dataset['key'] = new_key
    count = [1 for x in range(len(pos_dataset))]

    pos_dataset['count'] = count

    pivot_table_count = pos_dataset.pivot_table(index=["payment_origin", "name"], columns="key", aggfunc="count", values="count")
    pivot_table_count = pivot_table_count.fillna(' ')
    pivot_table_count = pivot_table_count.reset_index()
    pivot_table_count['new_payment_origin'] =  [ x+'-'+'Count_members_app' for x in pivot_table_count['payment_origin']]

    pivot_table_sum = pos_dataset.pivot_table(index=["payment_origin", "name"], columns="key", aggfunc="sum", values="expenses")
    pivot_table_sum = pivot_table_sum.fillna(' ')
    pivot_table_sum = pivot_table_sum.reset_index()
    pivot_table_sum['new_payment_origin'] =  [ x+'-'+'Sum_expenses' for x in pivot_table_sum['payment_origin']]

    #pivot_table.to_csv('pivot.csv', sep=',')


    data_concat = pd.concat([pivot_table_count, pivot_table_sum])
    data_concat = data_concat.reset_index()
    data_concat = data_concat.sort_values('payment_origin')
    del data_concat['index']
    data_concat = data_concat.reset_index()
    del data_concat['index']
    data_concat = data_concat.set_index(['payment_origin', 'new_payment_origin'])
    
    return data_concat
    

def main():
   app = QApplication(sys.argv)
   ex = App()
   sys.exit(app.exec_())
	
if __name__ == '__main__':
   main()