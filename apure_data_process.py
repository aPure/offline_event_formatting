# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'apure_app.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(400, 200)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setEnabled(True)
        self.centralwidget.setObjectName("centralwidget")
        self.stackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
        self.stackedWidget.setGeometry(QtCore.QRect(0, 0, 401, 181))
        self.stackedWidget.setObjectName("stackedWidget")
        self.welcome = QtWidgets.QWidget()
        self.welcome.setObjectName("welcome")
        self.stackedWidget.addWidget(self.welcome)
        self.offline_event = QtWidgets.QWidget()
        self.offline_event.setObjectName("offline_event")
        self.stackedWidget.addWidget(self.offline_event)
        self.purchase_products = QtWidgets.QWidget()
        self.purchase_products.setObjectName("purchase_products")
        self.stackedWidget.addWidget(self.purchase_products)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 400, 21))
        self.menubar.setObjectName("menubar")
        self.menuType = QtWidgets.QMenu(self.menubar)
        self.menuType.setObjectName("menuType")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionOffline_Event = QtWidgets.QAction(MainWindow)
        self.actionOffline_Event.setObjectName("actionOffline_Event")
        self.actionPurchase_type_by_product = QtWidgets.QAction(MainWindow)
        self.actionPurchase_type_by_product.setObjectName("actionPurchase_type_by_product")
        self.menuType.addAction(self.actionOffline_Event)
        self.menuType.addAction(self.actionPurchase_type_by_product)
        self.menubar.addAction(self.menuType.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "aPure Data Processor"))
        self.menuType.setTitle(_translate("MainWindow", "Type"))
        self.actionOffline_Event.setText(_translate("MainWindow", "Offline Event"))
        self.actionPurchase_type_by_product.setText(_translate("MainWindow", "Purchase type by product"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

