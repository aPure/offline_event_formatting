import os
from flask import Flask, render_template, request
import importlib.util
spec = importlib.util.spec_from_file_location(
    "formatting",
    "formatting.py"
)
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)

app = Flask(__name__)


APP_ROOT = os.path.dirname(os.path.abspath(__file__))

@app.route("/", methods=['GET'])
def index():
    return render_template('index.html', name='Offline Events')


@app.route("/upload", methods=['POST'])
def upload():
    file = request.files['excel_file']
    destination = "/".join([APP_ROOT, file.filename])
    file.save(destination)
    module.format(destination)
    return render_template("complete.html")     

if __name__ == '__main__':
    app.run(debug=True)