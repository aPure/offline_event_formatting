import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSlot
import pandas as pd
import re

def format(filename):
    purchases = pd.read_excel(filename)

    date = purchases[u'會員日期商品消費明細表']
    date = [d.replace('/', '-')+'T22:20:50Z' for d in date]
    date = [d.replace('107', '2018') for d in date]
    currency = ['TWD' for i in date]
    country = ['TW' for i in date]
    event_name = ['Purchase' for i in date]
    phone = purchases[u'會員日期商品消費明細表.1']
    phone = [re.sub(r'^09', '+8869', p) for p in phone]
    name = purchases[u'會員日期商品消費明細表.2']
    ln = [n for n in name]
    payment_origin = purchases[u'會員日期商品消費明細表.3']
    payment_number = purchases[u'會員日期商品消費明細表.5']
    #product = purchases[u'會員日期商品消費明細表.6']
    expense = purchases[u'會員日期商品消費明細表.12']
    order_id = purchases[u'會員日期商品消費明細表.5'] + '-' + purchases[u'會員日期商品消費明細表.3']
    dataset = pd.DataFrame({'event_time': date, 
                            'phone': phone, 
                        #'name': name, 
                            'payment_origin': payment_origin,
                            'ln': ln,
                        #'product': product,
                        'value': expense,
                        'order_id': order_id,
                        'country': country,
                        'currency': currency,
                        'event_name': event_name})

    dataset = dataset.loc[3:]
    dataset = dataset.loc[dataset['event_time'] != u'日期T22:20:50Z']
    dataset = dataset.loc[dataset['event_time'] != u'合計 :T22:20:50Z']
    dataset = dataset.loc[dataset['payment_origin'] != u'官網']
    dataset = dataset.loc[dataset['payment_origin'] != u'91APP移動官網']
    #dataset = dataset.loc[dataset['value'] > 0]
    del dataset['payment_origin']
    unique_phone = list(set(dataset['phone']))
    unique_dataset = pd.DataFrame()
    for p in unique_phone:
        subdat = dataset.loc[dataset['phone'] == p]
        values = subdat['value']  
        subdat.iloc[0]['value'] = sum(subdat['value'])
        unique_dataset = unique_dataset.append(subdat.iloc[0])
        #print(subdat.iloc[0]['value'])
    unique_dataset = unique_dataset.loc[unique_dataset['value'] > 0]
    return unique_dataset

def ptp(filename):
    purchases = pd.read_excel(filename)

    purchase_id = purchases[u'購物車編號']
    product_sku = purchases[u'商品料號']
    number_items = purchases[u'數量']

    # clean content
    cleaned_product_sku = []
    classified = []
    s10 = []
    v05 = []
    v08_09 = []
    for s in product_sku:
        cleaned_product_sku.append(s[1:])
        if s[1:].startswith('P') or s[1:].startswith('U'):
            classified.append('發熱衣') 
        elif s[1:].startswith('S'):
            classified.append('襪子') 
        elif s[1:].startswith('V01') or s[1:].startswith('V05'):
            classified.append('男內褲') 
        elif s[1:].startswith('V07') or s[1:].startswith('V08') or s[1:].startswith('V09'):
            classified.append('女內褲') 
        elif s[1:].startswith('T09'):
            classified.append('feGood') 
        else:
            classified.append('酸鹼平衡衣') 

    for s in product_sku:
        if s[1:].startswith('S10'):
            s10.append(s[1:])
        elif s[1:].startswith('V05'):
            v05.append(s[1:])
        elif s[1:].startswith('V08') or s[1:].startswith('V09'):
            v08_09.append(s[1:])
        else:
            pass
                
    dataset = pd.DataFrame({'id': purchase_id, 
                            'sku': cleaned_product_sku, 
                            'classified': classified, 
                            'number': number_items})

    dataset_s10 = dataset.loc[dataset['sku'].isin(s10)].copy()
    dataset_v05 = dataset.loc[dataset['sku'].isin(v05)].copy()
    dataset_v08_09 = dataset.loc[dataset['sku'].isin(v08_09)].copy()

    dataset_s10.loc[:,'classified'] = '蓄熱襪'
    dataset_v05.loc[:,'classified'] = '男三角褲'
    dataset_v08_09.loc[:,'classified'] = '女無痕褲'

    dataset = dataset.append(dataset_s10, ignore_index=True)
    dataset = dataset.append(dataset_v05, ignore_index=True)
    dataset = dataset.append(dataset_v08_09, ignore_index=True)

    #dataset.pivot(index="key", columns="classified")
    dataset = dataset.pivot_table(index="id", columns="classified", aggfunc="sum")
    dataset.fillna(' ')
    return dataset
    #new_filename = filename.split('.')[0]+'.csv'

    #dataset.to_csv(new_filename, sep=',', encoding='utf-8')

class Window(QMainWindow):
    def __init__(self): 
        super().__init__()
        self.title = "aPure Offline Event formatter"
        self.left = 300
        self.top = 300
        self.width = 400
        self.height = 150
        # this will hold various layouts
        # self.centralwidget = QWidget()
        # self.centralwidget.setEnabled(True)
        
        self.initWindow()

    def initWindow(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        # creating widgets
        self.stacked_layout = QStackedWidget(self)
        self.offline_event = App()
        self.stacked_layout.addWidget(self.offline_event)
        self.purchase_product = PurchaseTypeProduct()
        self.stacked_layout.addWidget(self.purchase_product)
        #self.setCentralWidget(self.purchase_product)
        hbox = QHBoxLayout(self)
        hbox.addWidget(self.leftlist)
        hbox.addWidget(self.stacked_layout)
        # self.stacked_layout.addWidget(self.app_widget)
        # set the central widget to display the layout
        #self.central_widget = QWidget()
        #self.central_widget.setLayout(self.stacked_layout)
        #self.setCentralWidget(self.central_widget)
        # filling up a menu bar
        bar = self.menuBar()
        # File menu
        file_menu = bar.addMenu('File')
        # adding actions to file menu
        offline_event_action = QAction('Offline Event', self)
        purchase_product_action = QAction('Purchase by type of products', self)
        file_menu.addAction(offline_event_action)
        file_menu.addAction(purchase_product_action)
        # use `connect` method to bind signals to desired behavior
        offline_event_action.triggered.connect(self.Openfile)

    @pyqtSlot()
    def Openfile(self):
        # filename = self.getFile()
        # if filename[0] != '':
        #     formated_file = format(filename[0])
        #     new_filename = filename[0].split('.')[0]+'.csv'
        #     formated_file.to_csv(new_filename, sep=',', encoding='utf-8', index=False)
        print("ok")
        #self.stacked_layout.setCurrentIndex(1)

class App(QWidget):
    def __init__(self):
        super().__init__()
        self.button_move_x = 140
        self.button_move_y = 50
        self.initGUI()

    def initGUI(self):
        button = QPushButton('Format Excel file', self)
        button.setToolTip('Click this to format the excel file')
        button.move(self.button_move_x,self.button_move_y)
        button.clicked.connect(self.on_click)

    def getFile(self):
        options = QFileDialog.Options()
        fname = QFileDialog.getOpenFileName(self, 'Choose file', options=options)
        if fname:
            return fname
            
    @pyqtSlot()
    def on_click(self):
        filename = self.getFile()
        if filename[0] != '':
            formated_file = format(filename[0])
            new_filename = filename[0].split('.')[0]+'.csv'
            formated_file.to_csv(new_filename, sep=',', encoding='utf-8', index=False)

class PurchaseTypeProduct(QWidget):
    def __init__(self):
        super().__init__()
        self.button1_move_x = 140
        self.button1_move_y = 20
        self.button2_move_x = 140
        self.button2_move_y = 60
        self.initGUI()

    def initGUI(self):
        button1 = QPushButton('With Members', self)
        button2 = QPushButton('With Non Members', self)
        button1.setToolTip('Click this to format the excel file')
        button2.setToolTip('Click this to format the excel file')
        button1.move(self.button1_move_x,self.button1_move_y)
        button2.move(self.button2_move_x,self.button2_move_y)
        button2.clicked.connect(self.on_click)

    def getFile(self):
        options = QFileDialog.Options()
        fname = QFileDialog.getOpenFileName(self, 'Choose file', options=options)
        if fname:
            return fname
            
    @pyqtSlot()
    def on_click(self):
        filename = self.getFile()
        if filename[0] != '':
            formated_file = format(filename[0])
            new_filename = filename[0].split('.')[0]+'.csv'
            formated_file.to_csv(new_filename, sep=',', encoding='utf-8', index=False)

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Window()
    ex.show()
    sys.exit(app.exec_())