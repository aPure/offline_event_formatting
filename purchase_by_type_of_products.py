import pandas as pd

def ptp(filename):
    purchases = pd.read_excel(filename)

    purchase_id = purchases[u'購物車編號']
    product_sku = purchases[u'商品料號']
    number_items = purchases[u'數量']

    # clean content
    cleaned_product_sku = []
    classified = []
    s10 = []
    v05 = []
    v08_09 = []
    for s in product_sku:
        cleaned_product_sku.append(s[1:])
        if s[1:].startswith('P') or s[1:].startswith('U'):
            classified.append('發熱衣') 
        elif s[1:].startswith('S'):
            classified.append('襪子') 
        elif s[1:].startswith('V01') or s[1:].startswith('V05'):
            classified.append('男內褲') 
        elif s[1:].startswith('V07') or s[1:].startswith('V08') or s[1:].startswith('V09'):
            classified.append('女內褲') 
        elif s[1:].startswith('T09'):
            classified.append('feGood') 
        else:
            classified.append('酸鹼平衡衣') 

    for s in product_sku:
        if s[1:].startswith('S10'):
            s10.append(s[1:])
        elif s[1:].startswith('V05'):
            v05.append(s[1:])
        elif s[1:].startswith('V08') or s[1:].startswith('V09'):
            v08_09.append(s[1:])
        else:
            pass
                
    dataset = pd.DataFrame({'id': purchase_id, 
                            'sku': cleaned_product_sku, 
                            'classified': classified, 
                            'number': number_items})

    dataset_s10 = dataset.loc[dataset['sku'].isin(s10)].copy()
    dataset_v05 = dataset.loc[dataset['sku'].isin(v05)].copy()
    dataset_v08_09 = dataset.loc[dataset['sku'].isin(v08_09)].copy()

    dataset_s10.loc[:,'classified'] = '蓄熱襪'
    dataset_v05.loc[:,'classified'] = '男三角褲'
    dataset_v08_09.loc[:,'classified'] = '女無痕褲'

    dataset = dataset.append(dataset_s10, ignore_index=True)
    dataset = dataset.append(dataset_v05, ignore_index=True)
    dataset = dataset.append(dataset_v08_09, ignore_index=True)

    #dataset.pivot(index="key", columns="classified")
    dataset = dataset.pivot_table(index="id", columns="classified", aggfunc="sum")
    dataset.fillna(' ')

    new_filename = filename.split('.')[0]+'.csv'

    dataset.to_csv(new_filename, sep=',', encoding='utf-8')


ptp('91AppData_Day.xlsx')