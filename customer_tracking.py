import pandas as pd

def customer_tracking(filename1, filename2, filename3):
    withMembers = pd.read_excel(filename1)
    withMembers_order_id = withMembers[u'會員日期商品消費明細表.5'] + '-' + withMembers[u'會員日期商品消費明細表.3']
    withMembers_order_id = [x.replace(' ', '') for x in withMembers_order_id]

    withMembers_dataset = pd.DataFrame({'date': withMembers[u'會員日期商品消費明細表'], 
                                'name': withMembers[u'會員日期商品消費明細表.2'],
                                        'payment_origin': withMembers[u'會員日期商品消費明細表.3'],
                                        'expenses': withMembers[u'會員日期商品消費明細表.12'],
                            'phone_or_id_customers': withMembers[u'會員日期商品消費明細表.1'],
                        'order_id': withMembers_order_id,
                            #'id': withMembers[u'會員日期商品消費明細表.5']
                        })

    withMembers_dataset = withMembers_dataset.loc[3:]
    withMembers_dataset = withMembers_dataset.loc[withMembers_dataset['date'] != '日期']
    withMembers_dataset = withMembers_dataset.loc[withMembers_dataset['date'] != '合計 :']
    withMembers_dataset = withMembers_dataset.loc[withMembers_dataset['payment_origin'] != '合計:']


    del withMembers_dataset['date']

    ## With Non Members
    withNonMembers = pd.read_excel(filename2)

    withNonMembers_order_id = withNonMembers[u'店櫃商品銷貨明細表.4'] + '-' + withNonMembers[u'店櫃商品銷貨明細表.5']
    withNonMembers_order_id = [x.replace(' ', '') for x in withNonMembers_order_id]
    empty_phone_or_id = []
    for x in withNonMembers_order_id:
        empty_phone_or_id.append(' ')
        
    withNonMembers_dataset = pd.DataFrame({'date': withNonMembers[u'店櫃商品銷貨明細表.3'], 
                                            'name': withNonMembers[u'店櫃商品銷貨明細表.6'],
                            'payment_origin': withNonMembers[u'店櫃商品銷貨明細表.5'],
                        'expenses': withNonMembers[u'店櫃商品銷貨明細表.12'],
                            'phone_or_id_customers': empty_phone_or_id,
                        'order_id': withNonMembers_order_id,
                            #'id': withNonMembers[u'店櫃商品銷貨明細表.4']
                        })

    withNonMembers_dataset = withNonMembers_dataset.loc[3:]
    withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['payment_origin'] != '小計:']
    withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['payment_origin'] != '合計:']
    withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['date'] != '小計 :']
    withNonMembers_dataset = withNonMembers_dataset.loc[withNonMembers_dataset['date'] != '合計 :']

    del withNonMembers_dataset['date']



    ## Append members in non members
    pos_dataset = withNonMembers_dataset.append(withMembers_dataset)

    ## Divide data in 2
    phone_or_id_customers_order_id = pd.DataFrame({
        'phone_or_id_customers': pos_dataset['phone_or_id_customers'],
        'order_id': pos_dataset['order_id'],
        'payment_origin': pos_dataset['payment_origin'],
        'name': pos_dataset['name']
    })
    del pos_dataset['phone_or_id_customers']
    del pos_dataset['payment_origin']
    del pos_dataset['name']

    ## Aggregate to have sum for each order 
    pos_dataset = pos_dataset.groupby('order_id').agg('sum')

    pos_dataset = pos_dataset.reset_index()

    ## Classify members and non members
    membership = []
    members = withMembers_dataset['order_id'].tolist()
    phone_or_id_customers = []
    payment_origin = []
    name = []
    for key in pos_dataset['order_id']:
        other_info = phone_or_id_customers_order_id.loc[phone_or_id_customers_order_id['order_id'] == key]
        other_info_phone = other_info.loc[other_info['phone_or_id_customers'] !=' ']
        if not other_info_phone.empty:
            phone_or_id_customers.append(other_info_phone['phone_or_id_customers'].iloc[0])     
        else:
            phone_or_id_customers.append(' ')
        payment_origin.append(other_info['payment_origin'].iloc[0])
        name.append(other_info['name'].iloc[0])
        
        if key in members:
            membership.append("member")
        else:
            membership.append("non member")

    pos_dataset['membership'] = membership 
    pos_dataset['payment_origin'] = payment_origin 
    pos_dataset['phone_or_id_customers'] = phone_or_id_customers 
    pos_dataset['name'] = name 

    del pos_dataset['order_id']
    ## Get customers who are registered to 91 app
    _91_customers = pd.read_excel(filename3)
    customers_phone = [x[1:] for x in _91_customers['手機號碼']]
    app_not_app = []

    ## Detect those this day who are registered 91 or not
    app_not_app = []
    for c in pos_dataset['phone_or_id_customers']:
        if c in customers_phone:
            app_not_app.append('APP')
        else:
            app_not_app.append('NOAPP')

    pos_dataset['91APP'] = app_not_app

    new_key = pos_dataset['membership'] +'-'+pos_dataset['91APP']

    pos_dataset['key'] = new_key
    count = [1 for x in range(len(pos_dataset))]

    pos_dataset['count'] = count

    pivot_table_count = pos_dataset.pivot_table(index=["payment_origin", "name"], columns="key", aggfunc="count", values="count")
    pivot_table_count = pivot_table_count.fillna(' ')
    pivot_table_count = pivot_table_count.reset_index()
    pivot_table_count['new_payment_origin'] =  [ x+'-'+'Count_members_app' for x in pivot_table_count['payment_origin']]

    pivot_table_sum = pos_dataset.pivot_table(index=["payment_origin", "name"], columns="key", aggfunc="sum", values="expenses")
    pivot_table_sum = pivot_table_sum.fillna(' ')
    pivot_table_sum = pivot_table_sum.reset_index()
    pivot_table_sum['new_payment_origin'] =  [ x+'-'+'Sum_expenses' for x in pivot_table_sum['payment_origin']]

    #pivot_table.to_csv('pivot.csv', sep=',')


    data_concat = pd.concat([pivot_table_count, pivot_table_sum])
    data_concat = data_concat.reset_index()
    data_concat = data_concat.sort_values('payment_origin')
    del data_concat['index']
    data_concat = data_concat.reset_index()
    del data_concat['index']
    data_concat = data_concat.set_index(['payment_origin', 'new_payment_origin'])
    
    new_filename1 = filename1.split('.')[0]
    new_filename2 = filename2.split('.')[0]
    new_filename = new_filename1 +'_'+ new_filename2 +'.csv'
    data_concat.to_csv(new_filename, sep=',', encoding='utf-8')


customer_tracking('withMembers.xlsx', 'withNonMembers.xlsx', '91data_addedNumbers_CustomersInfo.xlsx')