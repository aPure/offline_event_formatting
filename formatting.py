import pandas as pd
import re

def format(filename):
    purchases = pd.read_excel(filename)

    date = purchases[u'會員日期商品消費明細表']
    date = [d.replace('/', '-')+'T22:20:50Z' for d in date]
    date = [d.replace('107', '2018') for d in date]
    currency = ['TWD' for i in date]
    country = ['TW' for i in date]
    event_name = ['Purchase' for i in date]
    phone = purchases[u'會員日期商品消費明細表.1']
    phone = [re.sub(r'^09', '+8869', p) for p in phone]
    name = purchases[u'會員日期商品消費明細表.2']
    ln = [n for n in name]
    payment_origin = purchases[u'會員日期商品消費明細表.3']
    payment_number = purchases[u'會員日期商品消費明細表.5']
    #product = purchases[u'會員日期商品消費明細表.6']
    expense = purchases[u'會員日期商品消費明細表.12']
    order_id = purchases[u'會員日期商品消費明細表.5'] + '-' + purchases[u'會員日期商品消費明細表.3']
    dataset = pd.DataFrame({'event_time': date, 
                            'phone': phone, 
                        #'name': name, 
                            'payment_origin': payment_origin,
                            'ln': ln,
                        #'product': product,
                        'value': expense,
                        'order_id': order_id,
                        'country': country,
                        'currency': currency,
                        'event_name': event_name})

    dataset = dataset.loc[3:]
    dataset = dataset.loc[dataset['event_time'] != u'日期T22:20:50Z']
    dataset = dataset.loc[dataset['event_time'] != u'合計 :T22:20:50Z']
    dataset = dataset.loc[dataset['payment_origin'] != u'官網']
    dataset = dataset.loc[dataset['payment_origin'] != u'91APP移動官網']
    #dataset = dataset.loc[dataset['value'] > 0]
    del dataset['payment_origin']
    unique_phone = list(set(dataset['phone']))
    unique_dataset = pd.DataFrame()
    for p in unique_phone:
        subdat = dataset.loc[dataset['phone'] == p]
        values = subdat['value']  
        subdat.iloc[0]['value'] = sum(subdat['value'])
        unique_dataset = unique_dataset.append(subdat.iloc[0])
        #print(subdat.iloc[0]['value'])
    unique_dataset = unique_dataset.loc[unique_dataset['value'] > 0]
    #print(unique_dataset)
    # Problems: Same timestamps

    #print(purchases[u'會員日期商品消費明細表.12'])
    #print(unique_phone)
    new_filename = filename.split('.')[0]+'.csv'
    unique_dataset.to_csv(new_filename, sep=',', encoding='utf-8')